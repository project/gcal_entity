<?php

namespace Drupal\gcal_entity;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for gcal_entity.
 */
class GcalEntityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
